const board = document.querySelector('svg').getAttribute('viewBox')
const BOARD_WIDTH = board.split(' ')[2]
const BOARD_HEIGHT = board.split(' ')[3]
const rects = document.querySelectorAll('rect')
let selected
let index = 0
selectRect(rects[index])

document.addEventListener('keydown', handleKeypress)
rects.forEach(r => r.addEventListener('click', () => selectRect(r)))

function selectRect(rect) {
  rects.forEach(r => {
    r.id = ''
    r.style.fill = ''
  })
  selected = rect
  selected.id = 'selected'
  selected.style.fill = 'lightblue'
}

function handleKeypress(e) {
  e.preventDefault()
  switch (e.code) {
    case 'ArrowRight':
      right()
      break
    case 'ArrowLeft':
      left()
      break
    case 'ArrowUp':
      up()
      break
    case 'ArrowDown':
      down()
      break
    case 'Space':
      rotate()
      break
    case 'Enter':
      next()
      break
    case 'ShiftRight' || 'ShiftLeft':
      prev()
      break
  }
}

function right() {
  const x = Number(selected.getAttribute('x'))
  const width = Number(selected.getAttribute('width'))
  if (x + width < BOARD_WIDTH) selected.setAttribute('x', x + 1)
}
function left() {
  const x = Number(selected.getAttribute('x'))
  if (x > 0) selected.setAttribute('x', x - 1)
}
function up() {
  const y = Number(selected.getAttribute('y'))
  if (y > 0) selected.setAttribute('y', y - 1)
}
function down() {
  const y = Number(selected.getAttribute('y'))
  const height = Number(selected.getAttribute('height'))
  if (y + height < BOARD_HEIGHT) selected.setAttribute('y', y + 1)
}
function rotate() {
  const x = Number(selected.getAttribute('x'))
  const y = Number(selected.getAttribute('y'))
  const width = Number(selected.getAttribute('width'))
  const height = Number(selected.getAttribute('height'))
  selected.setAttribute('width', height)
  selected.setAttribute('height', width)
  if (x + height > BOARD_WIDTH) selected.setAttribute('x', BOARD_WIDTH - height)
  if (y + width > BOARD_HEIGHT) selected.setAttribute('y', BOARD_HEIGHT - width)
}
function next() {
  index = index + 1
  if (index >= rects.length) index = 0
  selectRect(rects[index])
}
function prev() {
  index = index - 1
  if (index < 0) index = rects.length - 1
  selectRect(rects[index])
}

function random() {
  console.log(Math.random())
}

function toggleInfo() {
  const info = document.querySelector('.js-info')
  if(info.dataset.visible === 'true') {
    info.style.display = 'none'
    info.dataset.visible = 'false'
  } else {
    info.style.display = ''
    info.dataset.visible = 'true'
  }
}
