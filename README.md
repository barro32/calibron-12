# Calibron 12 Puzzle

Try to position all pieces so none overlap
https://barro32.gitlab.io/calibron-12/

## Controls
`←` `↑` `→` `↓` to move selected piece
`space` to rotate	
`enter` `shift` to cycle through pieces
`click` to select a piece

### TODO
- ~~mobile support~~
- win state
- ~~rotate outside bounds~~
- drag and drop
- 3 black pieces
- lock pieces in place and don't include in piece cycle
